#My own makefile, thanks to Remi's help

# define which compiler to use
CXX=g++

# ROOT flag
ROOTCFLAGS   = $(shell root-config --cflags)

# compiler flags
CXXFLAGS= -ggdb3 -pipe -Wall $(ROOTCFLAGS) -mpreferred-stack-boundary=8 -O3

# includes
#INCLUDES=-I/home/resasst/anirvan/Installations/HEPMC2/include

# all library locations
ROOTLIBS     = $(shell root-config --libs) -lEG
#HEPLIBS      = -L/home/resasst/anirvan/Installations/HEPMC2/lib -lHepMCfio -lHepMC
#BOOSTLIBS    = -lboost_iostreams -lboost_system

# library
#LIBS= $(ROOTLIBS) $(HEPLIBS) $(BOOSTLIBS)
#LIBS= $(ROOTLIBS) $(BOOSTLIBS)
LIBS= $(ROOTLIBS)

# find all .cc files
SRCS = $(wildcard *.cc)

# remove .cc from filenames
PRGS = $(patsubst %.cc,%,$(SRCS))

all: $(PRGS)

# final g++ compile instruction
# '%' symbol goes over every entry in the list of source or executable files
%: %.cc
	$(CXX) $(CXXFLAGS) $(INCLUDES)  -o $@ $< $(LIBS)

#$(CXX) $(CXXFLAGS) $(INCLUDES)  -o Read_tree Read_tree.cc $(LIBS)

.PHONY: clean
clean:
	rm -rf $(PRGS)
