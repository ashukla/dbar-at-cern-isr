using namespace std;



//__________________________________________________________________________________________________________________________
double antideuteron_p0 (const double T)
{
	/**
	Function to calculate p0 (in GeV) for antideuterons from Diego's paremeterized functions
	*/
	
    double a = 89.6;   
    double b = 6.6;
    double c = 0.73;
    return 0.001*a/(1+exp(b-log(T)/c));
}




//__________________________________________________________________________________________________________________________
TH1D * get_1D_from_2D_histogram (const TH2D* h2D)
{
	/**
	For each x bin, integrate over all y bins,
	
	x-axis: pT
	y-axis: y
	For each pT bin, integrate over the chosen range in the y-axis.  Example:  (-0.5, 0.5)
	finally, return a 1D histogram as function of pT
	*/
    
	TH1D * h1D = new TH1D(Form("%s_1D_pT", h2D->GetName()), "", 30, 0, 3);
	for (int i = 0; i <= h2D->GetNbinsX(); i++)
	{
		double total_entries_in_ith_xbin = 0;
		for (int j = 0; j <= h2D->GetNbinsY(); j++)
		{
			if (h2D->GetYaxis()->GetBinCenter(j) < -0.5)
				continue;
			
			if (h2D->GetYaxis()->GetBinCenter(j) > 0.5)
				continue;

			total_entries_in_ith_xbin = total_entries_in_ith_xbin + h2D->GetBinContent(i, j);
		}
		
		// if (i == 1)
			// cout<<i<<", "<<total_entries_in_ith_xbin<<endl;
		
		h1D->SetBinContent( i, total_entries_in_ith_xbin);
		h1D->SetBinError( i, sqrt(h1D->GetBinContent(i)));
	}

	return h1D;
}



//__________________________________________________________________________________________________________________________
vector<double> GetAsymmErrors(double n, bool debug = false)
{
	/**
	Calculate the asymmetric error bars using
	https://www-cdf.fnal.gov/physics/statistics/notes/pois_eb.txt
	*/
	
	if (n == 0)
	{
		vector<double> result;
		result.push_back(0); //error_low
		result.push_back(0); //error_high
		
		if (debug == true)
			cerr<<"| Setting errors to zero."<<endl;
		return result;
	}
	else if (n < 10000)
	{
		vector<double> result;
		result.push_back(-0.5 + sqrt(n+0.25)); //error_low
		result.push_back(0.5 + sqrt(n+0.25)); //error_high
		
		return result;
	}
	else
	{
		vector<double> result;
		result.push_back(sqrt(n)); //error_low
		result.push_back(sqrt(n)); //error_high
		
		return result;
	}
}



//__________________________________________________________________________________________________________________________
TH1D * Normalize_1D_Histogram (const TH1D* h, double nEvts, int SetScaleFactorType = 0, bool diagnostics = false) //function to divide each bin's content with that bin's width
{
	TH1D * h1 = (TH1D*)h->Clone();
	
	if (diagnostics == true)
		cout<<h1->GetNbinsX()<<endl;
	
	double scaleFactor = 1;
	
	for (int i = 0; i <= h1->GetNbinsX(); i++)
	{
		if (h1->GetBinContent(i) > 0)
		{
			if (SetScaleFactorType == 0)                                         /* simple particle yield, d^2n/dydpT  */
            {
                scaleFactor = 1/(nEvts*h1->GetXaxis()->GetBinWidth(i));
                h1->SetName(Form("%s_yield", h1->GetName()));
            }
            else if (SetScaleFactorType == 1)                                    /* for invariant differential cross section  */
            {
				scaleFactor = 1/(nEvts*h1->GetXaxis()->GetBinWidth(i)*h1->GetXaxis()->GetBinCenter(i));
                h1->SetName(Form("%s_invXSec", h1->GetName()));
            }
			else
            {
                scaleFactor = 1/(nEvts);
                h1->SetName(Form("%s_normalized", h1->GetName()));
            }
				
				
			double bin_content = h1->GetBinContent(i);
			double bin_error = GetAsymmErrors(bin_content)[0];
			h1->SetBinContent( i, scaleFactor*bin_content);
			h1->SetBinError( i, scaleFactor*bin_error);
			
			if (diagnostics == true)
			{
				cout<<"Bin "<<i<<": Original bin-content: "<<h->GetBinContent(i)<<", New bin-content: "<<h1->GetBinContent(i)<<", New bin-error: "<<h1->GetBinError(i)<<", Original bin-width: "<<h->GetXaxis()->GetBinWidth(i)<<", New bin-width: "<<h1->GetXaxis()->GetBinWidth(i)<<endl;
			}
		}
	}
	
	if (diagnostics == true)
		cout<<"\n";
	
	return h1;
}




//__________________________________________________________________________________________________________________________
double GetIntegralTH2D (const TH2D* h1)
{
    //Should produce the same output as TH2D::Integral().
    
	double integral = 0;
	for (int i = 1; i <= h1->GetNbinsX(); i++)
	{
		for (int j = 1; j <= h1->GetNbinsY(); j++)
		{
			integral = integral + h1->GetBinContent(i, j);
		}
	}
	return integral;
}




//__________________________________________________________________________________________________________________________
TGraphAsymmErrors * GetTGraphAsymm(const TH1D* h, const TH1D* h_high, const TH1D* h_low)
{
	TH1D * h_copy = (TH1D*)h->Clone();
	TH1D * h_high_copy = (TH1D*)h_high->Clone();
	TH1D * h_low_copy = (TH1D*)h_low->Clone();
	
	std::vector<double> x;
	std::vector<double> x_low;
	std::vector<double> x_high;
	std::vector<double> y;
	std::vector<double> y_errorLow;
	std::vector<double> y_errorHigh;
	
	for (int i = 1; i <= h_copy->GetNbinsX(); i++)
	{
		x.push_back(h_copy->GetBinCenter(i));
		x_low.push_back(0);
		x_high.push_back(0);
		
		std::vector<double> unsorted_bin_Counts = {h_low_copy->GetBinContent(i), h_copy->GetBinContent(i), h_high_copy->GetBinContent(i)};
		if (std::is_sorted(unsorted_bin_Counts.begin(), unsorted_bin_Counts.end())) 
		{
			y_errorLow.push_back(unsorted_bin_Counts[1]-unsorted_bin_Counts[0]);
			y.push_back(unsorted_bin_Counts[1]);
			y_errorHigh.push_back(unsorted_bin_Counts[2]-unsorted_bin_Counts[1]);
		}
		else
		{
			cout<<"Bin contents in bin "<<i<<" are unsorted! low: "<<unsorted_bin_Counts[0]<<", middle: "<<unsorted_bin_Counts[1]<<", high: "<<unsorted_bin_Counts[2]<<". Sorting..."<<endl;
			sort(unsorted_bin_Counts.begin(), unsorted_bin_Counts.end());
			y_errorLow.push_back(unsorted_bin_Counts[1]-unsorted_bin_Counts[0]);
			y.push_back(unsorted_bin_Counts[1]);
			y_errorHigh.push_back(unsorted_bin_Counts[2]-unsorted_bin_Counts[1]);
		}
	}
	
	TVectorD tv_x(x.size(), &x[0]);
	TVectorD tv_x_high(x_high.size(), &x_high[0]);
	TVectorD tv_x_low(x_low.size(), &x_low[0]);
	TVectorD tv_y(y.size(), &y[0]);
	TVectorD tv_y_errorLow(y_errorLow.size(), &y_errorLow[0]);
	TVectorD tv_y_errorHigh(y_errorHigh.size(), &y_errorHigh[0]);
	
	TGraphAsymmErrors * G_sim = new TGraphAsymmErrors(tv_x, tv_y, tv_x_low, tv_x_high, tv_y_errorLow, tv_y_errorHigh);
	return G_sim;
}




//__________________________________________________________________________________________________________________________
void make_pT_YieldPlot(const std::vector<TH1D*> vHist, double axis_y1, double axis_y2, string file_name)
{
	TCanvas *c1 = new TCanvas("c1", "c1", 1200, 1200);
    Size_t marker_size = 2;
    
	gPad->SetGridx(1);
	gPad->SetGridy(1);
    gStyle->SetOptStat(0); // set stat box to show number of entries
    
	c1->SetLogy(1);
	c1->SetTopMargin(0.02);
	c1->SetRightMargin(0.03);
	c1->SetLeftMargin(0.16);
	c1->SetBottomMargin(0.14);
	
	auto H0 = vHist[0];
	H0->SetTitle("");
	H0->GetXaxis()->SetTitle("p_{T} [GeV/c]");
	H0->GetYaxis()->SetTitle("1/N_{collision} d^{2}N/dp_{T}dy [(GeV/c)^{-1}]");
	H0->GetXaxis()->SetTitleOffset(1.1);
	H0->GetYaxis()->SetTitleOffset(1.5);
	H0->GetXaxis()->SetTitleSize(0.05);
	H0->GetYaxis()->SetTitleSize(0.05);
	H0->SetAxisRange(axis_y1, axis_y2, "Y");

	H0->SetLineWidth(3);
	H0->SetLineColor(kTeal);
	H0->SetMarkerStyle(34);
	H0->SetMarkerColor(kTeal);
	H0->SetMarkerSize(marker_size);
	
	auto H3 = vHist[3];
	H3->SetLineWidth(3);
	H3->SetLineColor(kMagenta+3);
	H3->SetMarkerStyle(24);
	H3->SetMarkerColor(kMagenta+3);
	H3->SetMarkerSize(marker_size);
    
	auto H6 = vHist[6];
	H6->SetLineWidth(3);
	H6->SetLineColor(kBlue);
	H6->SetMarkerStyle(21);
	H6->SetMarkerColor(kBlue);
	H6->SetMarkerSize(marker_size);
	
	
	// Generate graph with a single error band
	auto G_sim = GetTGraphAsymm(H3, H6, H0); //GetTGraphAsymm(const TH1D* h, const TH1D* h_high, const TH1D* h_low)
	G_sim->SetTitle("");
	G_sim->GetXaxis()->SetTitle("p_{T} [GeV/c]");
    G_sim->GetYaxis()->SetTitle("1/N_{collision} d^{2}N/dp_{T}dy [(GeV/c)^{-1}]");
	G_sim->GetXaxis()->SetTitleOffset(1.1);
	G_sim->GetYaxis()->SetTitleOffset(1.5);
	G_sim->GetXaxis()->SetTitleSize(0.05);
	G_sim->GetYaxis()->SetTitleSize(0.05);
	
	G_sim->SetLineWidth(3);
	G_sim->SetLineColor(kBlue);
	G_sim->SetMarkerStyle(21);
	G_sim->SetMarkerColor(kBlue);
	G_sim->SetMarkerSize(3);
	G_sim->SetFillColorAlpha(kBlue, 0.30);
	G_sim->SetFillStyle(1001); //3004
	G_sim->GetXaxis()->SetLimits(0,3);
	G_sim->GetHistogram()->SetMaximum(axis_y2);   // along          
    G_sim->GetHistogram()->SetMinimum(axis_y1);  //   Y 
	G_sim->Draw("a3");
    

	TLegend * leg = new TLegend(0.6,0.85,0.96,0.97); //x1, y1, x2, y2
	leg->AddEntry(G_sim,"Gomez model", "f");
	leg->Draw();

	c1->Update();
	c1->SaveAs(file_name.c_str());
	delete c1;
}




//__________________________________________________________________________________________________________________________
void make_invXSec_plot(const std::vector<TH1D*> vHist, TGraphErrors *gISRdata, TGraphErrors *gKachelriess, double axis_y1, double axis_y2, string file_name)
{
	TCanvas *c1 = new TCanvas("c1", "c1", 1200, 1200);

	gPad->SetGridx(1);
	gPad->SetGridy(1);
    gStyle->SetOptStat(0); // set stat box to show number of entries
    
	c1->SetLogy(1);
	c1->SetTopMargin(0.02);
	c1->SetRightMargin(0.03);
	c1->SetLeftMargin(0.16);
	c1->SetBottomMargin(0.14);
	
	auto H2 = vHist[2]; //p0-Gomez - 10%
	auto H3 = vHist[3]; //p0-Gomez
	auto H4 = vHist[4]; //p0-Gomez + 10%
	
	// Generate graph with a single error band
	auto G_sim = GetTGraphAsymm(H3, H4, H2); //GetTGraphAsymm(const TH1D* h, const TH1D* h_high, const TH1D* h_low)
	G_sim->SetTitle("");
	G_sim->GetXaxis()->SetTitle("p_{T} [GeV/c]");
    G_sim->GetYaxis()->SetTitle("Ed^{3}#sigma/dp^{3} [mb/GeV^{2}]");  
	G_sim->GetXaxis()->SetTitleOffset(1.1);
	G_sim->GetYaxis()->SetTitleOffset(1.5);
	G_sim->GetXaxis()->SetTitleSize(0.05);
	G_sim->GetYaxis()->SetTitleSize(0.05);
	
	G_sim->SetLineWidth(3);
	G_sim->SetLineColor(kBlue);
	G_sim->SetMarkerStyle(21);
	G_sim->SetMarkerColor(kBlue);
	G_sim->SetMarkerSize(3);
	G_sim->SetFillColorAlpha(kBlue, 0.30);
	G_sim->SetFillStyle(1001); //3004
	G_sim->GetXaxis()->SetLimits(0.0,1.5);
	G_sim->GetHistogram()->SetMaximum(axis_y2);   // along          
    G_sim->GetHistogram()->SetMinimum(axis_y1);  //   Y 
	G_sim->Draw("a3");
	
	gISRdata->SetMarkerStyle(21);
	gISRdata->SetMarkerColor(kBlack);
	gISRdata->SetMarkerSize(1);
    gISRdata->Draw("same PE"); 
  
	gKachelriess->SetLineWidth(3);
	gKachelriess->SetLineColor(kOrange);
	gKachelriess->SetMarkerStyle(21);
	gKachelriess->SetMarkerColor(kOrange);
	gKachelriess->SetMarkerSize(3);   
	gKachelriess->SetFillColorAlpha(kOrange, 0.30);
	gKachelriess->SetFillStyle(1001); //3004
	gKachelriess->Draw("same E3");

	TLegend * leg = new TLegend(0.6,0.85,0.96,0.97); //x1, y1, x2, y2
	leg->AddEntry(gISRdata,"Data ISR", "p");
	leg->AddEntry(gKachelriess,"Kachelriess model", "f");
	leg->AddEntry(G_sim,"Coalescence model p_{0} #pm 10%", "f");
	leg->Draw();

	c1->Update();
	c1->SaveAs(file_name.c_str());
	delete c1;
}
