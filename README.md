# dbar-at-cern-isr

Author: Anirvan Shukla (anirvan@hawaii.edu)

This code investigates dbar production at CERN-ISR energies of 
sqrtS = 53 GeV (or pLab = 1500 GeV).

The following histograms need to be read: 
1. number of pp collisions      :    TH1D    "Hist_header"
2. dbar y-pT counts at p0-30%p0 :    TH2D    "Hist_pT_y_dbar_dbarp0_60.73"
3. dbar y-pT counts at p0-20%p0 :    TH2D    "Hist_pT_y_dbar_dbarp0_69.41"
4. dbar y-pT counts at p0-10%p0 :    TH2D    "Hist_pT_y_dbar_dbarp0_78.08"
5. dbar y-pT counts at p0       :    TH2D    "Hist_pT_y_dbar_dbarp0_86.76"
6. dbar y-pT counts at p0+10%p0 :    TH2D    "Hist_pT_y_dbar_dbarp0_95.43"
7. dbar y-pT counts at p0+20%p0 :    TH2D    "Hist_pT_y_dbar_dbarp0_104.11"
8. dbar y-pT counts at p0+30%p0 :    TH2D    "Hist_pT_y_dbar_dbarp0_112.78"


Steps:
1. Get the 7 different y-pT counts histograms from our large-scale simulation.
2. Select desirable y-bins.
3. Reduce 2D histogram in y-pT to 1D histogram in pT, after selecting a suitable rapidity bin range.
4. Normalize by nEvents and bin widths.
5. Generate final plot.




To use:
1. Type "make" to compile "investigate_sqrtS-53GeV.cc". You might have to use your own system's Makefile.
2. Create a new directory, via "mkdir plots".
2. Type "./investigate_sqrtS-53GeV".