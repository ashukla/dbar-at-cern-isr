/**
Author: Anirvan Shukla (anirvan@hawaii.edu)

This code investigates dbar production at CERN-ISR energies of 
sqrtS = 53 GeV (or pLab = 1500 GeV).

The following histograms need to be read: 
1. number of pp collisions      :    TH1D    "Hist_header"
2. dbar y-pT counts at p0-30%p0 :    TH2D    "Hist_pT_y_dbar_dbarp0_60.73"
3. dbar y-pT counts at p0-20%p0 :    TH2D    "Hist_pT_y_dbar_dbarp0_69.41"
4. dbar y-pT counts at p0-10%p0 :    TH2D    "Hist_pT_y_dbar_dbarp0_78.08"
5. dbar y-pT counts at p0       :    TH2D    "Hist_pT_y_dbar_dbarp0_86.76"
6. dbar y-pT counts at p0+10%p0 :    TH2D    "Hist_pT_y_dbar_dbarp0_95.43"
7. dbar y-pT counts at p0+20%p0 :    TH2D    "Hist_pT_y_dbar_dbarp0_104.11"
8. dbar y-pT counts at p0+30%p0 :    TH2D    "Hist_pT_y_dbar_dbarp0_112.78"


Steps:
Get the 7 different y-pT counts histograms from our large-scale simulation.
Select desirable y-bins.
Reduce 2D histogram in y-pT to 1D histogram in pT, after selecting a suitable rapidity bin range.
Generate yield or invariant crosssection plots by normalizing by nEvents/bin widths/etc.
Generate plots.
*/


#include <iostream>
#include <vector>

#include <TROOT.h>
#include <TMath.h>
#include <TFile.h>
#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TPaveStats.h>
#include <TPad.h>
#include <TPaletteAxis.h>
#include <TAttPad.h>
#include <TColor.h>
#include <TVectorD.h>

#include "include/Utilities.h"
#include "include/Data.h"

using namespace std;



//__________________________________________________________________________________________________________________________
int main(int argc, char* argv[])
{	
	//read histogram files from antihelium production
	TFile * infile = new TFile("inputfile/pp_antihe3_1500-GeV.root");
	
	
    //get energy bins and nEvents simulated per bin
    double T = 1500;
    TH1D * hEvents = (TH1D*)infile->Get("Hist_header"); 
	double nEvents = hEvents->GetEntries();
    cout<<endl<<nEvents<<" p+p collisions at pLab="<<T<<" GeV were recorded."<<endl;
    
    
    //define vector to store the seven histograms
    std::vector<TH2D*> vHist_pT_y_dbar;
    
    //get dbar y-pT histogram for each value of p0
    auto h_minus30 = (TH2D*)infile->Get(Form("Hist_pT_y_dbar_dbarp0_%.2f", 1000*(antideuteron_p0(T)-0.3*antideuteron_p0(T))));  vHist_pT_y_dbar.push_back(h_minus30);
    auto h_minus20 = (TH2D*)infile->Get(Form("Hist_pT_y_dbar_dbarp0_%.2f", 1000*(antideuteron_p0(T)-0.2*antideuteron_p0(T))));  vHist_pT_y_dbar.push_back(h_minus20);
    auto h_minus10 = (TH2D*)infile->Get(Form("Hist_pT_y_dbar_dbarp0_%.2f", 1000*(antideuteron_p0(T)-0.1*antideuteron_p0(T))));  vHist_pT_y_dbar.push_back(h_minus10);
    auto h_diego   = (TH2D*)infile->Get(Form("Hist_pT_y_dbar_dbarp0_%.2f", 1000*(antideuteron_p0(T)                       )));  vHist_pT_y_dbar.push_back(h_diego);
    auto h_plus10  = (TH2D*)infile->Get(Form("Hist_pT_y_dbar_dbarp0_%.2f", 1000*(antideuteron_p0(T)+0.1*antideuteron_p0(T))));  vHist_pT_y_dbar.push_back(h_plus10);
    auto h_plus20  = (TH2D*)infile->Get(Form("Hist_pT_y_dbar_dbarp0_%.2f", 1000*(antideuteron_p0(T)+0.2*antideuteron_p0(T))));  vHist_pT_y_dbar.push_back(h_plus20);
    auto h_plus30  = (TH2D*)infile->Get(Form("Hist_pT_y_dbar_dbarp0_%.2f", 1000*(antideuteron_p0(T)+0.3*antideuteron_p0(T))));  vHist_pT_y_dbar.push_back(h_plus30);
    cout<<int(vHist_pT_y_dbar.size())<<" histograms read from file "<<infile->GetName()<<".\n\n";
    
    
    //for calculating the dbar total production cross section
    double pp_int_xSection = 35.62; //This is the p+p interaction cross section at pLab=1500GeV, in mbarns.
    double norm_factor = 2.0*TMath::Pi()*1.*nEvents/pp_int_xSection;
    double dbar_prod_xSection[7];
    
    //get 1D histogram from 2D histograms, then normalize
    std::vector<TH1D*> vHist_pT_dbar;
	std::vector<TH1D*> vHist_pT_dbar_normalized;
    std::vector<TH1D*> vHist_invXsec_pT_dbar;
    
	for (int k = 0; k < int(vHist_pT_y_dbar.size()); k++)
	{
        double total_dbar_produced = vHist_pT_y_dbar[k]->Integral();
        // double total_dbar_produced = GetIntegralTH2D(vHist_pT_y_dbar[k]);
        
        dbar_prod_xSection[k] = pp_int_xSection*total_dbar_produced/nEvents;
        cout<<"Dbars produced =  "<<total_dbar_produced<<",  dbar total production cross section (mb) =  "<<dbar_prod_xSection[k]<<endl;
        
        //get 1D pT count distributions:
        vHist_pT_dbar.push_back(get_1D_from_2D_histogram(vHist_pT_y_dbar[k]));
        
        /* We choose the rapidity bin width to be 1 in get_1D_from_2D_histogram()
        So when dividing by rapidity bin width, nothing more needs to be done. */
        vHist_pT_dbar_normalized.push_back(Normalize_1D_Histogram(vHist_pT_dbar[k], nEvents, 0, false));
		vHist_invXsec_pT_dbar.push_back(Normalize_1D_Histogram(vHist_pT_dbar[k], norm_factor, 1, false));
	}
	cout<<"\nRatio total dbar production cross section: \nGomez/-10% =  "<<dbar_prod_xSection[3]/dbar_prod_xSection[2]<<"\n+10%/Gomez =  "<<dbar_prod_xSection[4]/dbar_prod_xSection[3]<<endl;
	
    
    
    /// get ISR data
    TGraphErrors * gISRdata = GetDataGraph_CERN_ISR();
	
    /// Kachelriess simulation
	TGraphErrors * gKachelriess = GetDataGraph_Kachelriess();
    
    ///make plot for pT distribution
    cout<<"\n\nPlotting..."<<endl;
    make_pT_YieldPlot(vHist_pT_dbar_normalized, 4e-9, 1e-4, "plots/hist_dbar_sqrtS-53-GeV.png");
	make_invXSec_plot(vHist_invXsec_pT_dbar, gISRdata, gKachelriess, 2e-6, 1e-2, "plots/hist_invXsec_dbar_sqrtS-53-GeV.png");
    
    cout<<"\nAll done. Exiting."<<endl;
    return 0;
}


